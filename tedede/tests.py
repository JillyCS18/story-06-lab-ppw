from django.test import TestCase, Client, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import homepage
from .models import Status
import datetime
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story_6_Unit_Test(TestCase):

    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_story_6_using_status_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Status.html')

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status = 'Lagi capek, banyak tugas, mau rebahan aja', datetime = datetime.datetime.now())

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_story_6_POST(self):
        Status.objects.create(status = 'Lagi capek, banyak tugas, mau rebahan aja', datetime = datetime.datetime.now())
        response = Client().post(reverse('tedede:homepage'), {'status': Status.objects.all().order_by('datetime')})
        self.assertEqual(response.status_code, 200)

        html_response = response.content.decode('utf-8')
        self.assertIn('Lagi capek', html_response)


class Story_6_Functional_Test(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)



    def tearDown(self):
        self.selenium.quit()
        super(Story_6_Functional_Test, self).tearDown()

    def test_story_6_can_submit_status_and_display_it(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_id('status')
        submit = selenium.find_element_by_tag_name('button')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(10)
        self.assertIn('Coba Coba', self.selenium.page_source)



        
