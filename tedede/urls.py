from django.urls import path
from . import views

app_name = "tedede"

urlpatterns = [
    path('', views.homepage, name='homepage'),
]
